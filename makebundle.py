#!/usr/bin/env python

import zipfile
import subprocess
import os
import sphinx.cmd.build
import re

filestoskip=[
    re.compile('.*~$'),
    re.compile('^\..*'),
    re.compile('.*\.py$'),
    re.compile('.*\.pyc$'),
    re.compile('^doc{0}.*'.format(os.path.sep)),
    re.compile('.*__pycache__.*'),
    re.compile('charmm-beta-.*\.zip'),
]


version=subprocess.check_output(['git', 'describe', '--tags']).decode('utf-8').strip()

sphinx.cmd.build.main(['-a', '-b', 'latex', os.path.join('doc','source'), os.path.join('doc','build','latex')])
cwd=os.getcwd()
try:
    os.chdir(os.path.join('doc', 'build', 'latex'))
    os.system('make')
finally:
    os.chdir(cwd)

sphinx.cmd.build.main(['-a', '-b', 'html', os.path.join('doc','source'), os.path.join('doc','build','html')])
sphinx.cmd.build.main(['-a', '-b', 'epub', os.path.join('doc','source'), os.path.join('doc','build','epub')])

with zipfile.ZipFile('charmm-beta-{}.zip'.format(version), 'w', compression=zipfile.ZIP_DEFLATED) as zf:
    rootdir = '.'
    for folder, subdirs, files in os.walk(rootdir):
        for filename in files:
            fullfilename = os.path.join(folder, filename)
            # remove the leading "./" or ".\\" from the archive name
            arcname=fullfilename[len(rootdir):]
            while arcname[0] in [os.path.sep, os.path.altsep]:
                arcname=arcname[1:]
            if any([regex.match(arcname) for regex in filestoskip]):
                continue
            arcname=os.path.join('charmm-beta.ff', arcname)
            zf.write(fullfilename, arcname)
    zf.write(os.path.join('doc', 'build','latex', 'charmm36m-beta.pdf'), 'charmm-beta-doc.pdf')
    zf.write(os.path.join('doc', 'build','epub', 'charmm36m-beta.epub'), 'charmm-beta-doc.epub')
    htmlbuildroot=os.path.join('doc','build','html')
    for folder, subdirs, files in os.walk(htmlbuildroot):
        for filename in files:
            fullfilename = os.path.join(folder, filename)
            arcname = fullfilename[len(htmlbuildroot):]
            while arcname[0] in [os.path.sep, os.path.altsep]:
                arcname=arcname[1:]
            arcname=os.path.join('charmm-beta-doc', arcname)
            zf.write(fullfilename, arcname)
