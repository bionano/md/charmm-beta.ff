A few words about β-peptides
============================

Structurally speaking, the difference between β-amino acids and their natural counterparts is only a methylene group in
the backbone. While they have many structural functional and properties in common with α-peptides and proteins, the
additional torsion angle lends these molecules unique and interesting features, simultaneously rendering common
molecular modeling tools developed for proteins unusable.

The present force field extension deals with acyclic β-amino acids and the peptides constructed from them. According to
the substitution site, four common variants exist:

.. figure:: _static/diamides.png
    :figwidth: 70%
    :align: center

    The four kinds of β-amino acids: bare β-backbone or β-alanine (a),
    β\ :sup:`2`\ -amino acid (b), β\ :sup:`3`\ -amino acid (c) and β\ :sup:`2,3`\ -amino acid (d)

In contrast to natural ones, β-amino acids do not have a single, universally accepted terminology. In the documentation
of this force field extension, we adopt the following, widely used convention, which emphasizes the homology with
α-amino acids and allows to account for the absolute conformation (chirality) as well. The general block format is :

``<chirality><substitution type>h<side-chain designation>``, where:

<chirality>
    is the designation of the chirality of the substitution site atoms. For monosubstituted β-amino acids, this is either
    (\ *S*\ ) or (\ *R*\ ), including parentheses. For disubstituted ones, the substitution site is also labelled
    to avoid ambiguity, i.e. (2\ *S*, 3\ *R*) etc.

<substitution type>:
    either β\ :sup:`2`\ , β\ :sup:`3`\ or β\ :sup:`2,3`\ .

<side-chain designation>:
    single-letter abbreviation of the proteinogenic amino-acid whose side-chain is referred to. As for the chirality above,
    in the case of disubstituted amino-acids the substitution site must be explicitly given in order to avoid
    confusion, i.e. (2A,3Q), etc.

We include α-amino acids in this notation, too, with the following scheme:
``<chirality>α<side-chain designation>``, where:


<chirality>
    is similar as for β\ :sup:`2`\ - or β\ :sup:`3`\ -amino acids, i.e. either
    (\ *S*\ ) or (\ *R*\ )

<side-chain designation>:
    is once again the single-letter abbreviation of proteinogenic amino acids

Examples:
    monosubstituted:
        * (\ *S*\ )β\ :sup:`2`\ hV: valine side-chain on the α-carbon with *S* chirality
        * (\ *R*\ )β\ :sup:`3`\ hR: arginine side-chain on the β-carbon with *R* chirality

    disubstituted:
        * (2\ *S*\ ,3\ *R*\ )β\ :sup:`2,3` h(2A,3L): disubstituted β-amino acid with an alanine side-chain on the α-carbon
          (with *S* chirality) and an arginine on the β-carbon (*R* chirality)

    achiral (bare backbone):
        * βA

    α-amino acids:
        * (\ *S*\ )αV: L-valine
        * (\ *R*\ )αW: D-tryptophan

Two, more complicated examples are shown in the next two figures:

.. figure:: _static/hairpin6.png
    :figwidth: 70%
    :align: center

    (2\ *R*\ ,3\ *S*\ )β\ :sup:`2,3`\ h(2A,3A) -
    (2\ *R*\ ,3\ *S*\ )β\ :sup:`2,3`\ h(2A,3V) -
    (\ *S*\ )β\ :sup:`2`\ hV -
    (\ *S*\ )β\ :sup:`3`\ hK -
    (2\ *R*\ ,3\ *S*\ )β\ :sup:`2,3`\ h(2A,3A) -
    (2\ *R*\ ,3\ *S*\ )β\ :sup:`2,3`\ h(2A,3L)

.. figure:: _static/valxval.png
    :figwidth: 70%
    :align: center

    (\ *S*\ )β\ :sup:`3`\ hV -
    (\ *S*\ )β\ :sup:`3`\ hA -
    (\ *S*\ )β\ :sup:`3`\ hL -
    (2\ *S*\ ,3\ *S*\ )β\ :sup:`2,3`\ h(2A,3A) -
    (\ *S*\ )β\ :sup:`3`\ hV -
    (\ *S*\ )β\ :sup:`3`\ hA -
    (\ *S*\ )β\ :sup:`3`\ hL

