Additional features
===================

Apart from supplying residue topologies and force field parameters for β-amino acids, this force field distribution
contains many other convenience features, listed in the following. While we supply them with a hope and feeling that
they will be useful, these are not as thoroughly tested as e.g. the force field parameters themselves. The user is
therefore cautioned to assess and test simulation results obtained by using any of these features.

Alternative terminal groups
---------------------------

- ACE: acetyl N-terminus, taken from CHARMM, should work

- BUT: butyryl N-terminus, constructed in analogy with ACE, not tested

Solvent topologies and boxes
----------------------------

β-peptides are frequently studied in non-aqueous milieu. Topologies and equilibrated boxes (T=300 K, p=1 bar) of some
common solvents are included for the usage in the solvation procedure of GROMACS (``gmx solvate`` and ``gmx pdb2gmx``):

- octanol:
    ``octanol.gro`` and ``octanol.itp``
- methanol:
    ``methanol.gro`` and ``methanol.itp``

Cyclic β-amino acids
--------------------

The residue topologies of two frequently used cyclic β-residues, 2-aminocyclopentanecarboxylic acid (ACPC) and
2-aminocyclohexanecarboxylic acid (ACHC) are also included. Technically, they are treated as disubstituted, i.e.
β\ :sup:`2,3`-amino residues, with the appropriate atom types for the backbone carbons (``CTA1`` for C\ :sub:`β` and
``CTB1`` for C\ :sub:`α`). Note that the dihedral angle barrier height parameters used were originally developed
and tested on β\ :sup:`2,3`-amino residues, and their validity and reliability has not been assessed.


Alternative parametrizations
----------------------------

In order to compare our proposed treatment of the β-backbone torsions to already available solutions, we tried implement
other force field parameter sets, too. The following alternative parameter sets are defined:

- ``BETA_ORIGINAL``: values for torsion parameters were chosen based on chemical analogy with already existing torsion
  parameters
- ``BETA_ZHUETAL``: Zhu and coworkers proposed a parameter set in their seminal work for treating the backbone torsions
  of β\ :sup:`3`-amino acids [Zhu2010]_. While the idea and method of parametrization is really good, we found the
  results basically unreproducible (key information missing from the paper and supporting information). While we did our
  best to guess the correct parameters, our implementation may or may not be the same as the original authors intended.
  Another shortcoming is, that they developed this extension for CHARMM22, therefore applying it to CHARMM36 might not
  fully be correct, to put it mildly.
- ``BETA_ZERO``: all optimized backbone torsion potential strength parameters are set to zero

If using GROMACS, by default the optimized parameters are used. One can select the other by giving the correct
preprocessor directive either in the topology (e.g. ``\# define BETA_ZHUETAL``) or in the .mdp file (e.g.
``define = -DBETA_ZHUETAL``)

.. .. [Zhu2010]  Zhu, Koenig, Hoffmann, Yethiraj, Cui. Journal of Computational Chemistry (2010).
    DOI: `10.1002/jcc.21493 <http://doi.wiley.com/10.1002/jcc.21493>`_






