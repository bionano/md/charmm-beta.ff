Reporting bugs and requesting features
======================================

If you find any inconsistencies, errors, unintended behaviour, please file a bug report at the `issues page`_ of the
`gitlab project`_.

.. _gitlab project: https://gitlab.com/awacha/charmm-beta.ff

.. _issues page:  https://gitlab.com/awacha/charmm-beta.ff/issues

