Quickstart
==========

Obtaining the force field files
-------------------------------

The project is hosted on Gitlab_. The most recent version, including this documentation, obtainable from there.

The preferred way to download the files is cloning the repository using git_:

.. code:: bash

    $ git clone https://gitlab.com/awacha/charmm-beta.ff.git charmm-beta.ff

This way the force field files can always be updated to the newest version with:

.. code:: bash

    $ cd charmm-beta.ff
    $ git pull origin master --tags

Versions are tagged in the repository, thus checking out a version is also straightforward:

.. code:: bash

    $ cd charmm-beta.ff
    $ git checkout <tag name>

The available versions can be listed:

.. code:: bash

    $ cd charmm-beta.ff
    $ git tag -l

Using with GROMACS
------------------

First ensure that the force field directory (``charmm-beta.ff``) is present as a subdirectory of any directory you
intend to execute the ``gmx`` command from (either copy or more preferably, symlink).

In order to make GROMACS recognize β-amino acid residues as Protein, the contents of the ``residuetypes_beta.dat`` file
in the force field directory must be appended to the ``residuetypes.dat`` file. The main file usually resides in
the ``top`` directory of GROMACS (usually ``/usr/share/gromacs/top``, or any directory pointed by the environment
variable ``$GMXTOP``). Instead of modifying this file, users are encouraged to create the updated version in their
working directory (from where the ``gmx`` commands will be run). E.g.:

.. code:: bash

    $ cat $GMXTOP/residuetypes.dat charmm-beta.ff/residuetypes_beta.dat > residuetypes.dat

If you have a molecular model of a β-peptide, you can create a topology with:

.. code:: bash

    $ gmx pdb2gmx -f betapeptide.g96 -ff charmm-beta -ter -water -o conf.g96 -p topol.top

Generating molecular geometries
-------------------------------

The present force field by itself does not support creating molecular geometries. However, we have made an `extension`_
to the PyMOL molecular graphics system which is able to build β-peptides in various conformations. You can find its
documentation at https://pmlbeta.readthedocs.io.



Using with CHARMM
-----------------

To be done...



.. _Gitlab: https://gitlab.com/awacha/charmm-beta.ff
.. _git: https://git-scm.com
.. _extension: https://gitlab.com/awacha/pmlbeta