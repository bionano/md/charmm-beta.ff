Parametrization
===============

.. note::
    This is just a short summary. Details for the interested can be found in our journal article [Wacha2019]_.

The most peculiar feature of β-peptides is their folding. In a similar fashion as for α-peptides and proteins, the
secondary structure is controlled and defined by:
- intrachain hydrogen bonding between the peptide bonds
- sidechain configuration
- backbone torsions

The first two are not expected to be different, but the third one is the one which puts β-peptides apart from their
natural analogues. Therefore, in order to make a force field developed and tested for proteins and α-peptides work on
β-peptides as well, backbone torsion parameters have to be re-parametrized.

The CHARMM force field family is parametrized against *ab initio* electronic structure simulations of simple molecules.

#TODO


.. [Zhu2010]  Zhu, Koenig, Hoffmann, Yethiraj, Cui. Journal of Computational Chemistry (2010)
    DOI: `10.1002/jcc.21493 <http://dx.doi.org/10.1002/jcc.21493>`_

.. [Wacha2019] Wacha, Nagy, Beke-Somfai. ChemPlusChem (2019) 84, 927-941.
    DOI: `10.1002/cplu.201900180 <http://dx.doi.org/10.1002/cplu.201900180>`_
