License
=======

Copyright (c) 2019, András Wacha, Tibor Nagy, Tamás Beke-Somfai

The use and distribution of this force field with or without modification is permitted provided all of the following
conditions are met:

- Unmodified redistributions must retain the above copyright notice, this list of conditions and the following
  disclaimer.

- Modified versions must also retain the copyright notice, the list of these conditions and the following disclaimer.
  Modifications must be clearly documented in an accompanying list of modifications.

- Neither the names of the authors and contributors, nor the name of their affiliations may be used to endorse or
  promote products derived from this work without specific prior written permission.

- When results obtained with this force field are published, references should be included to the original documentation
  for the sake of reproducibility.

Disclaimer
----------

THIS WORK IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR THEIR AFFILIATIONS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
